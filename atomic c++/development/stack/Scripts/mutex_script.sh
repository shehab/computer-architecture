#!/bin/bash

#make the file
cd ..

#clean
make clean

make mutex

echo "ROUND 1" >> Results/mutex
echo "ROUND 1" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 2" >> Results/mutex
echo "ROUND 2" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 3" >> Results/mutex
echo "ROUND 3" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 4" >> Results/mutex
echo "ROUND 4" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 5" >> Results/mutex
echo "ROUND 5" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 6" >> Results/mutex
echo "ROUND 6" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 7" >> Results/mutex
echo "ROUND 7" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 8" >> Results/mutex
echo "ROUND 8" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 9" >> Results/mutex
echo "ROUND 9" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"

echo "ROUND 10" >> Results/mutex
echo "ROUND 10" >> Results/Valgrind/mutex

#Run the program with 2 threads
echo "2 threads" >> Results/mutex
./main_mutex 2 5000000 >> Results/mutex

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 2 5000000 >> Results/Valgrind/mutex 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/mutex
./main_mutex 4 2500000 >> Results/mutex


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 4 2500000 >> Results/Valgrind/mutex 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/mutex
./main_mutex 8 1250000 >> Results/mutex


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 8 1250000 >> Results/Valgrind/mutex 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/mutex
./main_mutex 16 625000 >> Results/mutex


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 16 625000 >> Results/Valgrind/mutex 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/mutex
./main_mutex 32 312500 >> Results/mutex


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/mutex
valgrind ./main_mutex 32 312500 >> Results/Valgrind/mutex 2>&1

echo "32 threads done!"