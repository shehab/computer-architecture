touch opt_2_report_home

make clean
export OMP_NUM_THREADS=1
make
echo "________________________________________________________________________" >> opt_2_report_home
echo "input 1, 1 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input1 output1_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 2, 1 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input2 output2_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 3, 1 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input3 output3_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_1 0 >> opt_2_report_home
echo "" >> opt_2_report_home

make clean
export OMP_NUM_THREADS=2
make

echo "input 1, 2 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input1 output1_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 2, 2 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input2 output2_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 3, 2 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input3 output3_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_2 0 >> opt_2_report_home
echo "" >> opt_2_report_home

make clean
export OMP_NUM_THREADS=4
make

echo "input 1, 4 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input1 output1_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 2, 4 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input2 output2_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 3, 4 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input3 output3_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_4 0 >> opt_2_report_home
echo "" >> opt_2_report_home

make clean
export OMP_NUM_THREADS=8
make

echo "input 1, 8 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input1 output1_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 2, 8 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input2 output2_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 3, 8 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input3 output3_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_8 0 >> opt_2_report_home
echo "" >> opt_2_report_home

make clean
export OMP_NUM_THREADS=16
make

echo "input 1, 16 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input1 output1_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input1 output1_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 2, 16 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input2 output2_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input2 output2_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home

echo "input 3, 16 thread" >> opt_2_report_home
echo "________________________________________________________________________" >> opt_2_report_home
./nbody input3 output3_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
./nbody input3 output3_orig_16 0 >> opt_2_report_home
echo "" >> opt_2_report_home
