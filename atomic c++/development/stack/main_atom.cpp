#include "atomic_stack.cpp"
#include <thread>
#include <chrono>

#define INT

#ifdef INT
    stack<int> now_stack;
    int item = 5;
#endif
#ifndef INT
    stack<string> now_stack;
    string item = "blablablablablablablablablablablablablablablablablablablabla";
#endif


void thread_func(int pushes, int popps){
    
    for(int i = 0; i < pushes; i++) {
        now_stack.push(item);
    }

    for(int i = 0; i < popps; i++) {
        /*int a = **/now_stack.pop();
        // if(a) cout << a << endl;
    }
}


int main(int argc, char* argv[]){

    if (argc < 3) {
        cout << "Usage:" << endl;
        cout << "   " << argv[0] << " <n_threads> <tasks_per_thread>" << endl;
        return 0;
    }

    int num_threads = atoi(argv[1]);
    int num_tasks   = atoi(argv[2]);

    thread * threads = new thread[num_threads];

    auto start = chrono::system_clock::now();

    for(int i = 0; i < num_threads; i += 2){
        threads[i]      = thread(thread_func, num_tasks, 0);
        threads[i + 1]  = thread(thread_func, 0, num_tasks);
    }

    for(int i = 0; i < num_threads; i++){
        threads[i].join();
    }

    
    auto end = chrono::system_clock::now();

    delete [] threads;
    auto duration = chrono::duration_cast<chrono::microseconds>(end - start).count();

    cout << "Duration : " << duration << endl;
    return 0;
}