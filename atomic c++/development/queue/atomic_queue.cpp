#include <atomic>
#include <pthread.h>
#include <memory>
#include <iostream>

using namespace std;

template<typename T>

class queue {
    private:
        struct node {
            shared_ptr<T> data;
            node* next;
            node(T const& data_):data(new T(data_)){}
        };

        atomic<node*> head;
        atomic<node*> tail;
        atomic<bool> empty = 1;
        bool is_empty(){
            return !head && !tail ? 1 : 0;
        }
    public:
        void enqueue(T const& data){
            node* const new_node = new node(data);
            while(!empty.compare_exchange_weak(is_empty(), 0)){

            }
            tail->next = new_node;
            while(!tail->next.compare_exchange_weak(new_node, new_node));
        }
        shared_ptr<T> dequeue(){
            node* old_head = head.load();
            while(old_head && !head.compare_exchange_weak(old_head, old_head->next));
            return old_head ? old_head->data : shared_ptr<T>();
        }
	
};