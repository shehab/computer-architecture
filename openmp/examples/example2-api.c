#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

long num_steps = 100000;
double step = 0.1;;
int main() {
	int i, id;
	int n_threads = omp_get_num_threads();
	double pi;
	double *sum = (double*)malloc(n_threads*sizeof(double));

	for(i = 0, pi=0.0; i<n_threads; ++i) {
	  sum[i] = 0.0;
	}

	#pragma omp parallel private(id)
	{
	  double x;
	  id = omp_get_thread_num();
	  sum[id] = 0;
	  for(i=id;i<num_steps;i+=n_threads) {
		x = (i-0.5)*step;
		sum[id] += 2.0/(1.0+x*x);
	  }
	}
	
	for(i = 0, pi=0.0; i<n_threads; ++i) {
	  pi += step * sum[i];
	}
	printf("PI: %lf\n", pi);

	exit(0);
}
