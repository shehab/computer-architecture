#!/bin/bash

#make the file
cd ..

#clean
make clean

make atomic

echo "ROUND 1" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 2" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 3" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 4" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 5" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 6" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 7" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 8" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 9" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"

echo "ROUND 10" >> Results/atom

#Run the program with 2 threads
echo "2 threads" >> Results/atom
./main_atom 2 5000000 >> Results/atom

#Valgrind with 2 threads
echo "2 threads" >> Results/Valgrind/atom
valgrind ./main_atom 2 5000000 >> Results/Valgrind/atom 2>&1

echo "2 threads done!"

#Run the program with 4 threads
echo "4 threads" >> Results/atom
./main_atom 4 2500000 >> Results/atom


#Valgrind with 4 threads
echo "4 threads" >> Results/Valgrind/atom
valgrind ./main_atom 4 2500000 >> Results/Valgrind/atom 2>&1

echo "4 threads done!"

#Run the program with 8 threads
echo "8 threads" >> Results/atom
./main_atom 8 1250000 >> Results/atom


#Valgrind with 8 threads
echo "8 threads" >> Results/Valgrind/atom
valgrind ./main_atom 8 1250000 >> Results/Valgrind/atom 2>&1

echo "8 threads done!"

#Run the program with 16 threads
echo "16 threads" >> Results/atom
./main_atom 16 625000 >> Results/atom


#Valgrind with 16 threads
echo "16 threads" >> Results/Valgrind/atom
valgrind ./main_atom 16 625000 >> Results/Valgrind/atom 2>&1

echo "16 threads done!"

#Run the program with 32 threads
echo "32 threads" >> Results/atom
./main_atom 32 312500 >> Results/atom


#Valgrind with 32 threads
echo "32 threads" >> Results/Valgrind/atom
valgrind ./main_atom 32 312500 >> Results/Valgrind/atom 2>&1

echo "32 threads done!"