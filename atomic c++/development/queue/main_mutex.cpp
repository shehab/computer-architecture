#include <vector>
#include <thread>
#include <chrono>
#include "mutex_stack.cpp"

//for the values of last_action bool
#define ENGUEUEING 0
#define DEQUEUEING 1

//to define whether the last uneven thread enqueues or dequeues (first time it has to enqueue, so we set it to dequeue)
bool last_action = DEQUEUEING;

int num_threads = 0;
int num_tasks = 0;

int item = 5;

stack<int> now_stack;

// void special(){
//     int resEING ? results = *now_stack.dequeue() : ({now_stack.enqueue(item);0;});
//         last_action = 1 - last_action;
//     }
// }

void enqueuer(){
    for(int i = 0; i < num_tasks; i++) {
        now_stack.enqueue(item);
    }
}

void dequeuer(){
    for(int i = 0; i < num_tasks; i++) {
        now_stack.dequeue();
    }
}

int main(int argc, char* argv[]){

    if (argc < 3) {
        cout << "Usage:" << endl;
        cout << "   " << argv[0] << " <n_threads> <tasks_per_thread>" << endl;
        return 0;
    }

    num_threads = atoi(argv[1]);
    num_tasks = atoi(argv[2]);

    thread * threads = new thread[num_threads];

    auto start = chrono::system_clock::now();

    for(int i = 0; i < num_threads; i += 2){
        threads[i] = thread(enqueuer);
        threads[i + 1] = thread(dequeuer);
        //in case of an uneven thread number
        // if((num_threads - i) == 3){
        //     //sending out the last thread
        //     threads[i + 2] = thread(special);
        //     break;
        // }
    }


    for(int i = 0; i < num_threads; i++){
        threads[i].join();
    }

    delete [] threads;
    
    auto end = chrono::system_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(end - start).count();

    cout << "Duration : " << duration << endl;
    return 0;
}