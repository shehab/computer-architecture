#include <atomic>
#include <pthread.h>
#include <memory>
#include <iostream>

using namespace std;

template<typename T>
class stack
{
private:
	struct node
	{
		shared_ptr<T> data;
	    node* next;
		node(T const& data_):data(new T(data_)){} 
	};

	atomic<node*> head;
public:
	void push(T const& data)
	{
		node* const new_node=new node(data); 
		new_node->next=head.load(); 
		while(!head.compare_exchange_weak(new_node->next,new_node));
	}

	shared_ptr<T> pop()
	{
		node* old_head=head.load();
		while(old_head && !head.compare_exchange_weak(old_head,old_head->next));
		return old_head ? old_head->data : shared_ptr<T>();
	}
};


  /*THE PUSH

    new_node->next = head.load();

    while(head != new_node->next){  

        if (head == new_node->next){
            head = new_node;
            return 1;
        } else {
            new_node->next = head.load();
            return 0;
        }

    }
*/


 /*THE POP
    
    node* old_head = head.load();
    while (old_head && head != old_head){

        if(head == old_head){
            head = old_head->next;
            return 1;
        } else {
            old_head = head.load();
            return 0;
        }


    }*/
