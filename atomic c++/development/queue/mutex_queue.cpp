#include <pthread.h>
#include <memory>
#include <iostream>
#include <mutex>

using namespace std;

template<typename T>
class queue
{
	private:
		struct node
			{
				shared_ptr<T> data;
				node* next;
				node(T const& data_):data(new T(data_)){}
			};

		mutex queue_mutex;
		node* tail;
		node* head;
		
public:
	/*
	*	This method adds a new node in attribute tail
	*	unless queue is empty, in that case head & tal = new node
	*/
	void enqueue(T const& data)
	{
		node* const new_node = new node(data); 
		queue_mutex.lock();
		!tail ? head = new_node : tail->next = new_node;
		// if(!tail)	head = new_node;
		// else	tail->next = new_node;

		tail = new_node;
		queue_mutex.unlock();
	}

	/*
	*	This method extracts the node which is in head position
	*	and return the data it contained
	*/
	std::shared_ptr<T> dequeue()
	{
		queue_mutex.lock();
		node* old_head = head;
		if(!head)
		{
			queue_mutex.unlock();
			return shared_ptr<T>;
		}else
		{
			head = old_head->next;
			queue_mutex.unlock();
			return old_head->data;
		}
		return -1;
	}
};
