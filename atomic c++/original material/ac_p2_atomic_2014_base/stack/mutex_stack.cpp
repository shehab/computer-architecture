#include <pthread.h>
#include <memory>
#include <iostream>
#include <mutex>


template<typename T>
class stack
{
private:
	struct node
	{
		std::shared_ptr<T> data;
		node* next;
		node(T const& data_):data(new T(data_)){} 
	};

	
	std::mutex stack_mutex;
	node* head;
public:
	void push(T const& data)
	{
		node* const new_node=new node(data); 
		stack_mutex.lock();
		new_node->next=head;
		head = new_node; 
		stack_mutex.unlock();
        }
	std::shared_ptr<T> pop()
	{
		node* res;
		stack_mutex.lock();
		node* old_head=head;
		if(!head){
			stack_mutex.unlock();
			return std::shared_ptr<T>();
		}else{
			head = old_head->next;
			stack_mutex.unlock();
			return old_head->data;
		}
	}
};

