#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

long num_steps = 100000;
double step = 0.1;;
int main() {
	int i;
	double pi, x, sum = 0.0;
	#pragma omp parallel for \
		reduction(+: sum) private(x)
	for(i = 0; i < num_steps; ++i) {
		x = (i-0.5)*step;
		sum += 2.0/(1.0+x*x);
	}
	pi = step * sum;

	printf("PI: %lf\n", pi);
	exit(0);
}
